import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './components/Routes';
import { LoginProvider } from './components/LoginContext';
import './App.css'

function App() {
  return (
    <div className="App">
      <Router>
        <LoginProvider>
          <Routes/>
        </LoginProvider>
      </Router>
    </div>
  );
}

export default App;