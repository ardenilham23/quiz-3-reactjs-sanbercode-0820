import React from 'react'
import {HomeProvider} from './HomeContext'
import HomeLists from './HomeLists'
import './Home.css';

const Home = () => {
    return (
        <HomeProvider>
            <HomeLists></HomeLists>
        </HomeProvider>
        
    )
}

export default Home;
