import React, { useContext }  from "react";
import { Link, useHistory} from 'react-router-dom';
import { HeaderContext } from "./HeaderContext";
import { LoginContext } from "./LoginContext";
import Logo from '../images/logo.png';
import './Header.css';

const NavigationList = () =>{
    const history = useHistory()
    const [changeColorTheme,setChangeColorTheme] = useContext(HeaderContext);
    const [statusLogin, setStatusLogin, , , , ] = useContext(LoginContext);

    const handleLogOut = () => {
        setStatusLogin(0);
        alert('Logout Berhasil!')
        history.push("/")
    }

    return(
        <>
            <header>
                <div className={`navigation ${changeColorTheme ? "normal":"ocean"}`}>
                    <img id="logo" src={Logo} width="200px" alt="Logo Sanbercode"/>
                    <nav>
                        <ul>
                            {
                                statusLogin === 1 ? (
                                    <>
                                        <li id="home">
                                            <Link to="/">Home</Link>
                                        </li>
                                        <li id="about">
                                            <Link to="/about">About</Link>
                                        </li>
                                        <li id="logout" onClick={handleLogOut}>Logout</li>
                                        <li id="movie-list">
                                            <Link to="/movie-list">Movie List Editor</Link>
                                        </li>
                                    </>
                                ) : (
                                    <>
                                        <li id="home">
                                            <Link to="/">Home</Link>
                                        </li>
                                        <li id="about">
                                            <Link to="/about">About</Link>
                                        </li>
                                        <li id="login">
                                            <Link to="/login">Login</Link>
                                        </li>
                                    </>
                                )
                            }
                            <li>
                                <button className="button-theme" onClick={()=>setChangeColorTheme(!changeColorTheme)} style={{marginLeft: '15px', marginTop: '10px'}}> Change Theme </button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
        </>
    )
}

export default NavigationList