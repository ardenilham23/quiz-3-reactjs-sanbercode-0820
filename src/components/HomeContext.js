import React, { useState, useEffect, createContext } from 'react'
import axios from 'axios';

export const HomeContext = createContext()

export const HomeProvider = props => {
    const [dataHome, setDataHome] = useState(null);
    const [inputTitle, setInputTitle] = useState("");
    const [inputImage, setInputImage] = useState("");
    const [inputDescription, setInputDescription] = useState("");
    const [inputYear, setInputYear] = useState("");
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")
    const [selectedId, setSelectedId] = useState(0)
    const [statusForm, setStatusForm] = useState("create")

    useEffect(() => {
        if(dataHome === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
                .then(res => {
                    setDataHome(res.data.sort((a, b) => (a.rating > b.rating ? -1: 1)).map(el => { return { id: el.id, title: el.title, image: el.image_url, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating } }))
                })
        }
    }, [dataHome])

    return (
        <HomeContext.Provider value={
            {
                dataContext: [dataHome, setDataHome],
                titleContext: [inputTitle, setInputTitle],
                imageContext: [inputImage, setInputImage],
                descriptionContext: [inputDescription, setInputDescription],
                yearContext: [inputYear, setInputYear],
                durationContext: [inputDuration, setInputDuration],
                genreContext: [inputGenre, setInputGenre],
                ratingContext: [inputRating, setInputRating],
                idContext: [selectedId, setSelectedId],
                formContext: [statusForm, setStatusForm]
            }
        }>
            {props.children}
        </HomeContext.Provider>
    )
}



export default HomeContext
