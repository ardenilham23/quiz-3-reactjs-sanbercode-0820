import React from 'react'
import {HomeProvider} from './HomeContext'
import MovieLists from './MovieLists'
import MovieForm from './MovieForm'
import './Movie.css';

const Movie = () => {
    return (
        <HomeProvider>
            <MovieLists />
            <MovieForm />
        </HomeProvider>
    )
}

export default Movie;
