import React from 'react';
import './About.css';

const About = () => {
    return (
        <div className="content">
            <h1>Data Peserta Sanbercode Bootcamp ReactJs</h1>
            <p>1. <b>Nama</b> : Arden Ridho Ilham Syawalaxa</p>
            <p>2. <b>Email</b> : ardenilham23@gmail.com</p>
            <p>3. <b>Sistem Operasi yang digunakan</b> : Windows 8.1</p>
            <p>4. <b>Akun Gitlab</b> : https://gitlab.com/ardenilham23</p>
            <p>5. <b>Akun Telegram</b> : @namaku123</p>
        </div>
        
    )
}

export default About;
