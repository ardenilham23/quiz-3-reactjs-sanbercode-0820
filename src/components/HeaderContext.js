import React, { useState, createContext} from "react";

export const HeaderContext = createContext();

export const HeaderProvider = props => {
    const [changeColorTheme, setChangeColorTheme] = useState(true);

    
    return (
    <HeaderContext.Provider value={[changeColorTheme, setChangeColorTheme]}>
        {props.children}
    </HeaderContext.Provider>);
};